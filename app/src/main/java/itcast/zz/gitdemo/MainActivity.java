package itcast.zz.gitdemo;

import android.databinding.DataBindingUtil;
import android.icu.text.UnicodeSetSpanner;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import itcast.zz.gitdemo.databinding.ActivityMainBinding;
import okhttp3.OkHttpClient;

public class MainActivity extends AppCompatActivity {

    private Button btn;
    private OkHttpClient ok = new OkHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding mainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        mainBinding.setEvent(new EventListener() {
            @Override
            public void click1(View v) {
                Toast.makeText(MainActivity.this,"click1",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void click2(View v) {
                Toast.makeText(MainActivity.this,"click1",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void cilck3(String s) {
                Toast.makeText(MainActivity.this,s,Toast.LENGTH_SHORT).show();
            }
        });

        mainBinding.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sentToast(v);
            }
        });
    }
    public void sentToast(View view){
        Toast.makeText(MainActivity.this,"main",Toast.LENGTH_SHORT).show();
    }
}
